document.getElementById('myForm').addEventListener('submit',saveBookmark);

function saveBookmark(e){
	e.preventDefault();
	//get the values
	var site_name=document.getElementById('siteName').value;
	var site_url=document.getElementById('url').value;

	//make it a object
	bookmark={
		name: site_name,
		url: site_url
	};

	if (!siteName || !site_url){
		alert('please fill in the form')
		return false

	}

	var expression = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi;
	var regex = new RegExp(expression);

	if (!site_url.match(regex)){
		alert('enter a valid url');
		return false;
	}

	//store in local storage

	if (localStorage.getItem('bookmarks') === null){
		var bookmarks=[];
		bookmarks.push(bookmark);
		localStorage.setItem('bookmarks',JSON.stringify(bookmarks));
	}else{
		var bookmarks=JSON.parse(localStorage.getItem('bookmarks'));
		bookmarks.push(bookmark);
		localStorage.setItem('bookmarks',JSON.stringify(bookmarks));
	}
	fetchBookmarks()
	
}

// delete bookmark function 
function deleteBookmark(url)
{
	var bookmarks=JSON.parse(localStorage.getItem('bookmarks'));
	for (var i=0; i< bookmarks.length; i++){
		if (bookmarks[i].url==url){
			bookmarks.splice(i,1);
		}
	}
	localStorage.setItem('bookmarks',JSON.stringify(bookmarks));
	fetchBookmarks()
}


//fetch the data from local storage
function fetchBookmarks(){
	var bookmarks=JSON.parse(localStorage.getItem('bookmarks'));
	//get 
	var bookmarkresults=document.getElementById('bookmarksResults')

	bookmarkresults.innerHTML='';
	for (var i=0; i< bookmarks.length; i++){
		var name=bookmarks[i].name
		var url=bookmarks[i].url
		bookmarkresults.innerHTML+=
										// '<div class="well"> \
		// 								<div>\
		// 									<span style="color:blue;font-weight:bold">'
		// 										+name +
		// 									'</span>\
		// 									<span></br>'
		// 										+url+
		// 									'</span> \
		// 								<a class="btn btn-primary" target="_blank" href="'+url+'">Visit</a>\
		// 								</div>'
									// '<a class="btn btn-primary" target="_blank" href="'+url+'">Visit</a>'+
									// +' <button type="text" onclick="deleteBookmark(\''+url+'\')" class="btn btn-danger">Delete</button>'
									// +
									// '</h3>'+
									// '</div>';
									'<div class="saved">\
										<p class="lead">'+name+'</p>\
										<hr class="my-4">\
										<p>'+url+'</p>\
										<a class="btn btn-primary " href="'+url+'" role="button">Visit</a>\
										<button type="text" onclick="deleteBookmark(\''+url+'\')" class="btn btn-danger float-right">Delete</button>\
									</div>'


	}

}